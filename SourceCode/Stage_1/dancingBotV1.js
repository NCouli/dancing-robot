function createCube(){
    return [
        // X,    Y,     Z        R,        G,        B
		// Top - #4a31ff
		-1.0 ,  1.0 , -1.0  ,   0.29    , 0.192   , 1.0,
		-1.0 ,  1.0 ,  1.0  ,   0.29    , 0.192   , 1.0,
		 1.0 ,  1.0 ,  1.0  ,   0.29    , 0.192   , 1.0,
		 1.0 ,  1.0 , -1.0  ,   0.29    , 0.192   , 1.0,
		// Left - #112697
		-1.0 ,  1.0 ,  1.0  ,   0.066   , 0.149    , 0.592,
		-1.0 , -1.0 ,  1.0  ,   0.066   , 0.149    , 0.592,
		-1.0 , -1.0 , -1.0  ,   0.066   , 0.149    , 0.592,
		-1.0 ,  1.0 , -1.0  ,   0.066   , 0.149    , 0.592,
		// Right - #1E90FF
		1.0  ,  1.0 ,  1.0  ,   0.117   , 0.564    , 1.0,
		1.0  , -1.0 ,  1.0  ,   0.117   , 0.564    , 1.0,
		1.0  , -1.0 , -1.0  ,   0.117   , 0.564    , 1.0,
		1.0  ,  1.0 , -1.0  ,   0.117   , 0.564    , 1.0,
		// Front - #4169e1
		 1.0 ,  1.0 ,  1.0  ,   0.254   , 0.411    , 0.882,
		 1.0 , -1.0 ,  1.0  ,   0.254   , 0.411    , 0.882,
		-1.0 , -1.0 ,  1.0  ,   0.254   , 0.411    , 0.882,
		-1.0 ,  1.0 ,  1.0  ,   0.254   , 0.411    , 0.882,
		// Back - #0059ff
		 1.0 ,  1.0 , -1.0  ,   0.0     , 0.35     , 1.0,
		 1.0 , -1.0 , -1.0  ,   0.0     , 0.35     , 1.0,
		-1.0 , -1.0 , -1.0  ,   0.0     , 0.35     , 1.0,
		-1.0 ,  1.0 , -1.0  ,   0.0     , 0.35     , 1.0,
		// Bottom - #2c41af
		-1.0 , -1.0 , -1.0  ,   0.172   , 0.254    , 1.0,
		-1.0 , -1.0 ,  1.0  ,   0.172   , 0.254    , 1.0,
		 1.0 , -1.0 ,  1.0  ,   0.172   , 0.254    , 1.0,
		 1.0 , -1.0 , -1.0  ,   0.172   , 0.254    , 1.0
	];
}

function createIndices(){
    return [
		// Top
		0, 1, 2,
		0, 2, 3,
		// Left
		5, 4, 6,
		6, 4, 7,
		// Right
		8, 9, 10,
		8, 10, 11,
		// Front
		13, 12, 14,
		15, 14, 12,
		// Back
		16, 17, 18,
		16, 18, 19,
		// Bottom
		21, 20, 22,
		22, 20, 23
	];
}

function canvasDims(){
    var minDimension = Math.min(window.innerHeight, window.innerWidth);
    canvas.width = 0.95 * minDimension;
	canvas.height = 0.95 * minDimension;
}

function createGLContext() {
	var gl = canvas.getContext('webgl');
	if (!gl)
		console.log('WebGL not supported, falling back on experimental-webgl');
		gl = canvas.getContext('experimental-webgl');

	if (!gl)
		alert('Your browser does not support WebGL');
    return gl;
}

function sceneSettings(){
    gl.clearColor(0.5, 0.5, 0.5, 1.0);  // Grey color
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);
	gl.frontFace(gl.CCW);
	gl.cullFace(gl.BACK);
}

function createCompileShader(shaderType, shaderSource){
    var outShader = gl.createShader(shaderType);
    gl.shaderSource(outShader, shaderSource);
    gl.compileShader(outShader);
    if (!gl.getShaderParameter(outShader, gl.COMPILE_STATUS)){
        alert("Shader compilation error. " + gl.getShaderInfoLog(outShader));
        gl.deleteShader(outShader);
        outShader = null;
    }
    return outShader;
}

function initShaders(){
    var vertexShaderText = document.getElementById("vShader").textContent;
    var fragmentShaderText = document.getElementById("fShader").textContent;
    var vertexShader = createCompileShader(gl.VERTEX_SHADER, vertexShaderText);
    var fragmentShader = createCompileShader(gl.FRAGMENT_SHADER, fragmentShaderText);
    var program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);
	if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
		console.error('ERROR linking program!', gl.getProgramInfoLog(program));
		return;
	}
    return program;
}

function createBindBufferData(bufferType, arrType, drawType){
    var BufferObject = gl.createBuffer();  // For cube vertex
	gl.bindBuffer(bufferType, BufferObject);
	gl.bufferData(bufferType, arrType, drawType);
}

function initBuffers() {
    createBindBufferData(gl.ARRAY_BUFFER, new Float32Array(boxVertices), gl.STATIC_DRAW);
    createBindBufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(boxIndices), gl.STATIC_DRAW);
    var positionAttribLocation = gl.getAttribLocation(program, 'vertPosition');
	var colorAttribLocation = gl.getAttribLocation(program, 'vertColor');
    gl.vertexAttribPointer(
		positionAttribLocation, // Attribute location
		3, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		false,
		6 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0 // Offset from the beginning of a single vertex to this attribute
	);

	gl.vertexAttribPointer(
		colorAttribLocation, // Attribute location
		3, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		false,
		6 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		3 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
	);

	gl.enableVertexAttribArray(positionAttribLocation);
	gl.enableVertexAttribArray(colorAttribLocation);
}

function draw(vAngle, coordinateX, coordinateY, coordinateZ, fPlane){  
	var matViewUniformLocation = gl.getUniformLocation(program, 'mView');
	var matProjUniformLocation = gl.getUniformLocation(program, 'mProj');

    var worldMatrix = new Float32Array(16);
	var viewMatrix = new Float32Array(16);
	var projMatrix = new Float32Array(16);

	glMatrix.mat4.lookAt(viewMatrix, [coordinateX, coordinateY, coordinateZ], [0, 0, 0], [0, 0, 1]); 
	glMatrix.mat4.perspective(projMatrix, glMatrix.glMatrix.toRadian(vAngle), canvas.clientWidth / canvas.clientHeight, 0.01, fPlane);
	gl.uniformMatrix4fv(matViewUniformLocation, gl.FALSE, viewMatrix);
	gl.uniformMatrix4fv(matProjUniformLocation, gl.FALSE, projMatrix);
    
	gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
    gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);
}


function drawAgain(){ 
	viewDistance = document.getElementById("viewDistanceTxt").value;
	viewAngle = document.getElementById("viewAngleTxt").value;
	farPlane = 20 * viewDistance;
	
	// Έλεγχος για κενά text box
	if(viewAngle.length == 0){
		alert("Κενό πλαίσιο! Συμπληρώστε τη γωνία θέασης");
		draw(90, 7, 7, 7, 50.0);
		return;
	}

	if(viewDistance.length == 0){ 
		alert("Κενό πλαίσιο! Συμπληρώστε την απόσταση από την αρχή των αξόνων");
		draw(90, 7, 7, 7, 50.0);
		return;
	}
	
	draw(viewAngle, viewDistance, viewDistance, viewDistance, farPlane);
	
	if (document.getElementById('left').checked)
		if (document.getElementById('front').checked)
			if (document.getElementById('top').checked)
				draw(viewAngle, -viewDistance,-viewDistance,viewDistance, farPlane);
			else
				draw(viewAngle, -viewDistance,-viewDistance,-viewDistance, farPlane);
		else
			if (document.getElementById('top').checked)
				draw(viewAngle, -viewDistance,viewDistance,viewDistance, farPlane);
			else
				draw(viewAngle, -viewDistance,viewDistance,-viewDistance, farPlane);
	else
		if (document.getElementById('front').checked)
			if (document.getElementById('top').checked) 
				draw(viewAngle, viewDistance,-viewDistance,viewDistance, farPlane);
			else 
				draw(viewAngle, viewDistance,-viewDistance,-viewDistance, farPlane);
		else
			if (document.getElementById('top').checked) 
				draw(viewAngle, viewDistance,viewDistance,viewDistance, farPlane);
			else
				draw(viewAngle, viewDistance,viewDistance,-viewDistance, farPlane);
}


var gl, program, canvas, boxVertices, boxIndices;

function main(){
    canvas = document.getElementById("sceneCanvas");
	boxVertices = createCube();
    boxIndices = createIndices();
    canvasDims();
    gl = WebGLDebugUtils.makeDebugContext(createGLContext());
    sceneSettings();
    program = initShaders();
    initBuffers();
    gl.useProgram(program);
    draw(90, 7, 7, 7, 50.0);
}