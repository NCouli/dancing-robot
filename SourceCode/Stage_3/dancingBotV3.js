function createCube(){
    return [
		//      Axis          
        // X,    Y,     Z     
		// Top 
		-1.0 ,  1.0 , -1.0  , 0, 0,
		-1.0 ,  1.0 ,  1.0  , 0, 1,
		 1.0 ,  1.0 ,  1.0  , 1, 1,
		 1.0 ,  1.0 , -1.0  , 1, 0,
		// Left
		-1.0 ,  1.0 ,  1.0  , 0, 0,
		-1.0 , -1.0 ,  1.0  , 0, 1,
		-1.0 , -1.0 , -1.0  , 1, 1,
		-1.0 ,  1.0 , -1.0  , 1, 0,
		// Right
		1.0  ,  1.0 ,  1.0  , 0, 0,
		1.0  , -1.0 ,  1.0  , 0, 1,
		1.0  , -1.0 , -1.0  , 1, 1,
		1.0  ,  1.0 , -1.0  , 1, 0,
		// Front
		 1.0 ,  1.0 ,  1.0  , 0, 0,
		 1.0 , -1.0 ,  1.0  , 0, 1,
		-1.0 , -1.0 ,  1.0  , 1, 1,
		-1.0 ,  1.0 ,  1.0  , 1, 0,
		// Back
		 1.0 ,  1.0 , -1.0  , 0, 0,
		 1.0 , -1.0 , -1.0  , 0, 1,
		-1.0 , -1.0 , -1.0  , 1, 1,
		-1.0 ,  1.0 , -1.0  , 1, 0,
		// Bottom
		-1.0 , -1.0 , -1.0  , 0, 0,
		-1.0 , -1.0 ,  1.0  , 0, 1,
		 1.0 , -1.0 ,  1.0  , 1, 1,
		 1.0 , -1.0 , -1.0  , 1, 0
	];
}

function createIndices(){
    return [
		// Top
		0, 1, 2,
		0, 2, 3,
		// Left
		5, 4, 6,
		6, 4, 7,
		// Right
		8, 9, 10,
		8, 10, 11,
		// Front
		13, 12, 14,
		15, 14, 12,
		// Back
		16, 17, 18,
		16, 18, 19,
		// Bottom
		21, 20, 22,
		22, 20, 23
	];
}
function canvasDims(){
    var minDimension = Math.min(window.innerHeight, window.innerWidth);
    canvas.width = 0.95 * minDimension;
	canvas.height = 0.95 * minDimension;
}

function createGLContext() {
	var gl = canvas.getContext('webgl');
	if (!gl)
		console.log('WebGL not supported, falling back on experimental-webgl');
		gl = canvas.getContext('experimental-webgl');

	if (!gl)
		alert('Your browser does not support WebGL');
    return gl;
}

function sceneSettings(){
    gl.clearColor(0.5, 0.5, 0.5, 1.0);  // Grey color
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);
	gl.frontFace(gl.CCW);
	gl.cullFace(gl.BACK);
}

function createCompileShader(shaderType, shaderSource){
    var outShader = gl.createShader(shaderType);
    gl.shaderSource(outShader, shaderSource);
    gl.compileShader(outShader);
    if (!gl.getShaderParameter(outShader, gl.COMPILE_STATUS)){
        alert("Shader compilation error. " + gl.getShaderInfoLog(outShader));
        gl.deleteShader(outShader);
        outShader = null;
    }
    return outShader;
}

function initShaders(){
    var vertexShaderText = document.getElementById("vShader").textContent;
    var fragmentShaderText = document.getElementById("fShader").textContent;
    var vertexShader = createCompileShader(gl.VERTEX_SHADER, vertexShaderText);
    var fragmentShader = createCompileShader(gl.FRAGMENT_SHADER, fragmentShaderText);
    var program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);
	if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
		console.error('ERROR linking program!', gl.getProgramInfoLog(program));
		return;
	}
    return program;
}

function createBindBufferData(bufferType, arrType, drawType){
    var BufferObject = gl.createBuffer();  // For cube vertex
	gl.bindBuffer(bufferType, BufferObject);
	gl.bufferData(bufferType, arrType, drawType);
}
var boxTexture;
function createTexture(textureObject) {	// Responsible for textures
	boxTexture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, boxTexture);
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
}

function applyTexture(textureObject){
	gl.texImage2D(
		gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,
		gl.UNSIGNED_BYTE,
		document.getElementById(textureObject)
	);
	
	gl.bindTexture(gl.TEXTURE_2D, boxTexture);
	gl.activeTexture(gl.TEXTURE0);
}

function initBuffers() {
    createBindBufferData(gl.ARRAY_BUFFER, new Float32Array(boxVertices), gl.STATIC_DRAW);
    createBindBufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(boxIndices), gl.STATIC_DRAW);
    var positionAttribLocation = gl.getAttribLocation(program, 'vertPosition');
	var texCoordAttribLocation = gl.getAttribLocation(program, 'aTextureCoordinates');
	gl.vertexAttribPointer(
		positionAttribLocation, // Attribute location
		3, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		false,
		5 * Float32Array.BYTES_PER_ELEMENT , // Size of an individual vertex
		0 // Offset from the beginning of a single vertex to this attribute
	);
	gl.vertexAttribPointer(
		texCoordAttribLocation, // Attribute location
		2, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		false,
		5 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		3 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
	);

	gl.enableVertexAttribArray(positionAttribLocation);
	gl.enableVertexAttribArray(texCoordAttribLocation);
	
	createTexture('metal');
	createTexture('pada');
	createTexture('floor');
	createTexture('sky')
}

function scaleNtransf(scaleVec, transformVec){	// Scales and translates
	var scalingMatrix = new Float32Array(16);
	var translationMatrix = new Float32Array(16);
	var firstStageVec = glMatrix.mat4.create();
	var finalStageVec = glMatrix.mat4.create();
	var projectionMatrix = glMatrix.mat4.create();
	glMatrix.mat4.fromScaling(scalingMatrix, scaleVec);
    glMatrix.mat4.fromTranslation(translationMatrix, transformVec);

	glMatrix.mat4.multiply(firstStageVec, translationMatrix, scalingMatrix);
    glMatrix.mat4.multiply(finalStageVec, projectionMatrix, firstStageVec);

	return finalStageVec;
}

function drawRobot(){
	var posUniformLocation = gl.getUniformLocation(program, 'matrix');
	var finalStageVec;
	//----------------------------------------------------------------------------
	finalStageVec = scaleNtransf([2.0, 3.0, 1.0], [3, 0.0, 1.0]);
	gl.uniformMatrix4fv(posUniformLocation, false, finalStageVec);

	applyTexture('metal');


    gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);	// Left foot
	//-----------------------------------------------------------------------------

	finalStageVec = scaleNtransf([2.0, 3.0, 1.0], [-3.0, 0.0, 1.0]);
	gl.uniformMatrix4fv(posUniformLocation, false, finalStageVec);
	gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);	// Right foot
	//-----------------------------------------------------------------------------
	
	finalStageVec = scaleNtransf([2.0, 1.5, 4.0], [3.0, -1.5, 6.0]);
	gl.uniformMatrix4fv(posUniformLocation, false, finalStageVec);
	gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);	// Left leg
	//-----------------------------------------------------------------------------
	
	finalStageVec = scaleNtransf([2.0, 1.5, 4.0], [-3.0, -1.5, 6.0]);
	gl.uniformMatrix4fv(posUniformLocation, false, finalStageVec);
	gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);	// Right leg
	//-----------------------------------------------------------------------------
	
	finalStageVec = scaleNtransf([1.0, 2.0, 5.0], [6.0, -1.5, 17.0]);
	gl.uniformMatrix4fv(posUniformLocation, false, finalStageVec);
	gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);	// Left hand
	//-----------------------------------------------------------------------------
	
	finalStageVec = scaleNtransf([1.0, 2.0, 5.0], [-6.0, -1.5, 17.0]);
	gl.uniformMatrix4fv(posUniformLocation, false, finalStageVec);
	gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);	// Right hand
	//-----------------------------------------------------------------------------

	finalStageVec = scaleNtransf([5.0, 3.0, 6.0], [0.0, -1.5, 16.0]);
	gl.uniformMatrix4fv(posUniformLocation, false, finalStageVec);
	gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);	// Core
	//-----------------------------------------------------------------------------
	applyTexture('pada');
	finalStageVec = scaleNtransf([3.0, 2.0, 2.5], [0.0, -1.5, 24.0]);
	gl.uniformMatrix4fv(posUniformLocation, false, finalStageVec);
	gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);	// Head
	//-----------------------------------------------------------------------------
	applyTexture('floor');
	finalStageVec = scaleNtransf([25.0, 25.0, 1/20], [0.0, 0.0, 0.0]);
	gl.uniformMatrix4fv(posUniformLocation, false, finalStageVec);
	gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);	// Floor
	//-----------------------------------------------------------------------------
	//applyTexture('sky');
	//finalStageVec = scaleNtransf([50, 30, 50], [5.0, 5.0, 10.0]);
	//gl.uniformMatrix4fv(posUniformLocation, false, finalStageVec);
	//gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);	// Skybox
	
}

function draw(vAngle, coordinateX, coordinateY, coordinateZ, fPlane){ 
    var matWorldUniformLocation = gl.getUniformLocation(program, 'mWorld');
	var matProjUniformLocation = gl.getUniformLocation(program, 'mProj');
	var matViewUniformLocation = gl.getUniformLocation(program, 'mView');

    var worldMatrix = new Float32Array(16);
	var viewMatrix = new Float32Array(16);
	var projMatrix = new Float32Array(16);

	glMatrix.mat4.identity(worldMatrix);
	glMatrix.mat4.lookAt(viewMatrix, [coordinateX, coordinateY, coordinateZ], [0, 0, 0], [0, 0, 1]); 
	glMatrix.mat4.perspective(projMatrix, glMatrix.glMatrix.toRadian(vAngle), canvas.clientWidth / canvas.clientHeight, 0.01, fPlane);

    gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);
	gl.uniformMatrix4fv(matViewUniformLocation, gl.FALSE, viewMatrix);
	gl.uniformMatrix4fv(matProjUniformLocation, gl.FALSE, projMatrix);

	gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

	drawRobot();
}

function drawAgain(){ 
	viewDistance = document.getElementById("viewDistanceTxt").value;
	viewAngle = document.getElementById("viewAngleTxt").value;
	farPlane = 20 * viewDistance;
	
	// Έλεγχος για κενά text box
	if(viewAngle.length == 0){
		alert("Κενό πλαίσιο! Συμπληρώστε τη γωνία θέασης");
		draw(90, 7, 7, 7, 50.0);
		return;
	}

	if(viewDistance.length == 0){ 
		alert("Κενό πλαίσιο! Συμπληρώστε την απόσταση από την αρχή των αξόνων");
		draw(90, 7, 7, 7, 50.0);
		return;
	}
	
	draw(viewAngle, viewDistance, viewDistance, viewDistance, farPlane);
	
	if (document.getElementById('left').checked)
		if (document.getElementById('front').checked)
			if (document.getElementById('top').checked)
				draw(viewAngle, -viewDistance,-viewDistance,viewDistance, farPlane);
			else
				draw(viewAngle, -viewDistance,-viewDistance,-viewDistance, farPlane);
		else
			if (document.getElementById('top').checked)
				draw(viewAngle, -viewDistance,viewDistance,viewDistance, farPlane);
			else
				draw(viewAngle, -viewDistance,viewDistance,-viewDistance, farPlane);
	else
		if (document.getElementById('front').checked)
			if (document.getElementById('top').checked) 
				draw(viewAngle, viewDistance,-viewDistance,viewDistance, farPlane);
			else 
				draw(viewAngle, viewDistance,-viewDistance,-viewDistance, farPlane);
		else
			if (document.getElementById('top').checked) 
				draw(viewAngle, viewDistance,viewDistance,viewDistance, farPlane);
			else
				draw(viewAngle, viewDistance,viewDistance,-viewDistance, farPlane);
}


var gl, program, canvas, boxVertices, boxIndices, viewDistance;

function main(){
    canvas = document.getElementById("sceneCanvas");
	boxVertices = createCube();
    boxIndices = createIndices();
    canvasDims();
    gl = WebGLDebugUtils.makeDebugContext(createGLContext());
    sceneSettings();
    program = initShaders();
    initBuffers();
    gl.useProgram(program);
    draw(90, 30, 30, 30, 1000.0);
}

var requestID=0;

var totalAngle = 2;
var totalZ = 7;

function startAnimation() {
    if (requestID == 0)
		viewDistance = 90;
		var numStepAngle = 1;
		var numStepZ = 5;
        requestID = window.requestAnimationFrame(animationStep);
}

function animationStep () {
	numStepAngle = parseFloat(document.getElementById("stepAngleTxt").value); 
	numStepZ = parseFloat(document.getElementById("stepZTxt").value); 
	
	numStepAngle = numStepAngle*Math.PI/180.0; 
	totalAngle += numStepAngle; 
	totalZ += numStepZ; 	

    var matWorldUniformLocation = gl.getUniformLocation(program, 'mWorld');
	var matProjUniformLocation = gl.getUniformLocation(program, 'mProj');
	var matViewUniformLocation = gl.getUniformLocation(program, 'mView');

    var worldMatrix = new Float32Array(16);
	var viewMatrix = new Float32Array(16);
	var projMatrix = new Float32Array(16);

    gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

	var x = viewDistance*Math.cos(totalAngle);  
	var y = viewDistance*Math.sin(totalAngle);
	glMatrix.mat4.lookAt(viewMatrix,[x,y,totalZ],[0,0,0],[0,0,1]);
	
	
	glMatrix.mat4.perspective(projMatrix,Math.PI/4,1,0.01,1000);
	glMatrix.mat4.identity(worldMatrix);

	gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);
	gl.uniformMatrix4fv(matViewUniformLocation, gl.FALSE, viewMatrix);
	gl.uniformMatrix4fv(matProjUniformLocation, gl.FALSE, projMatrix);

    drawRobot();
    requestID = window.requestAnimationFrame(animationStep);
}

function stopAnimation() {
    window.cancelAnimationFrame(requestID);
    requestID = 0;
}